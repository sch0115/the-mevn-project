const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;

let _db;

const mongoConnect = callback => {
  MongoClient.connect(
    'mongodb+srv://sch0115:Passw0rd01@cluster0-sih4m.mongodb.net/test?retryWrites=true',
    { useNewUrlParser: true }
  )
    .then(client => {
      console.log('Connected!');
      _db = client.db();
      callback();
    })
    .catch(err => {
      console.log(err);
    });
};

exports.mongoConnect = mongoConnect;
exports.getDb = function () {
  return _db;
};
