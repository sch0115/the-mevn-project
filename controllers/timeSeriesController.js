const TimeSeriesRecord = require('../models/timeSeriesRecord');

const saveTimeSeriesData = data => {
  const timeSeriesRecord = new TimeSeriesRecord(data);
  timeSeriesRecord.save();
  console.log(timeSeriesRecord);
};

exports.saveTimeSeriesData = saveTimeSeriesData;