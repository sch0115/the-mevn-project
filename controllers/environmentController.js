var CronJob = require('cron').CronJob;
const environmentService = require('../services/environmentService');
const timeSeriesService = require('../services/timeSeriesService');

const environmentCheck = new CronJob('0/10 * * * * *', function() {
  console.log('Starting regular 10s check...');
  const environmentState = environmentService.readEnvironmentState();
  //TODO update environment model?
  timeSeriesService.saveTimeSeriesData(environmentState)
}, null, true);


//
// function initialize() {
//   console.log('Initializing environmental controller...', ' !!!NIY!!!');
//   environmentCheck.start();
// }