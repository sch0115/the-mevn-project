const mongo = require('../util/database');

module.exports = class TimeSeriesRecord {
  constructor(data) {
    this.created = new Date();
    this.temperature = data.temperature;
    this.humidity = data.humidity;
    this.mainLightOn = data.mainLightOn;
    this.humifierOn = data.humifierOn;
    this.test = 'test';
  }

  save() {
    const db = mongo.getDb();
    if(db) {
      db.collection('timeSeries')
        .insertOne(this)
        .then(result => {
          console.log('inserted ', this, ' into DB');
        })
        .catch(err => console.log(err));
    } else{
      console.error('Database not connected! Environment data were not saved!')
    }
  }

  static getAll() {
    console.log('thisRuns');
  }
}


