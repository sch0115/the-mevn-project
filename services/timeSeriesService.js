const TimeSeriesRecord = require('../models/timeSeriesRecord');

const saveTimeSeriesData = data => {
  const timeSeriesRecord = new TimeSeriesRecord(data);
  timeSeriesRecord.save();
};

exports.saveTimeSeriesData = saveTimeSeriesData;