var express = require('express');
var router = express.Router();
const environmentController = require('../controllers/environmentController');

/* GET listing. */
router.get('/', function(req, res, next) {
  res.send('Welcome to environment endpoint!');
});
/* POST listing. */
router.post('/', function(req, res, next) {
  res.status(202);
  res.end();
});

module.exports = router;