var express = require('express');
var router = express.Router();
const House = require('../models/house');

/* GET new home page. */
router.get('/', async function (req, res, next) {
  const houses = await House.getNewHouses();
  res.render('propertyChecker', {houses});
});
/* GET favourites page. */
router.get('/favourites', async function (req, res, next) {
  const houses = await House.getFavouriteHouses();
  res.render('propertyChecker', {houses});
});
/* GET tabled page. */
router.get('/tabled', async function (req, res, next) {
  const houses = await House.getHousesInTable();
  res.render('propertyChecker', {houses});
});
/* GET hidden page. */
router.get('/hidden', async function (req, res, next) {
  const houses = await House.fetchAll();
  res.render('propertyChecker', {houses});
});
/* GET home page. */
router.post('/hide', async function (req, res, next) {
     // update house with the ID in req.body
  // todo add "get house by ID" (not static update house)
  await House.hide(req.body._id);
  const houses = await House.getNewHouses();
  console.log(houses.length);
  res.render('propertyChecker', {houses});
});/* GET home page. */
router.post('/save', async function (req, res, next) {
  //update house with the ID in req.body
  // todo add "get house by ID" (not static update house)
  // todo add "makeFavourite" into house class
  await House.update(req.body._id, {favourite: true, checked: false});
  const houses = await House.getNewHouses();
  console.log(houses.length);
  res.render('propertyChecker', {houses});
});

router.post('/table', async function (req, res, next) {
  //update house with the ID in req.body
  // todo add "get house by ID" (not static update house)
  // todo add "makeFavourite" into house class
  await House.update(req.body._id, {inTable: true, checked: true, favourite: false});
  const houses = await House.getFavouriteHouses();
  console.log(houses.length);
  res.render('propertyChecker', {houses});
});

module.exports = router;
