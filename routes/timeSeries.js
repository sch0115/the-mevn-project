var express = require('express');
var router = express.Router();
const timeSeriesController = require('../controllers/timeSeriesController');

/* GET listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});
/* POST listing. */
router.post('/', function(req, res, next) {
  timeSeriesController.saveTimeSeriesData(req.body);
  res.status(200);
  res.end();
});

module.exports = router;
